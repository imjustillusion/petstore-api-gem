lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'petstore'

client = Petstore::Client.new('special-key') #
#pet =  client.pet.add name: 'doggie', catalog: { name:'name'}, tags: [{name: 'another tag'}], status: 'boring', photoUrls: ['/path/to/img.jpg']
pet =  client.pet.add name: 'doggie', catalog: {name: 'catalog'}, tags: ['another tag'], status: 'available', photoUrls: ['/path/to/img.jpg']
#puts pet[:response].status
#puts pet.inspect
#
pet2 = client.pet.update pet[:id], name: 'a little doggie'
puts pet2.inspect

#file = client.pet.set_image pet["id"], 'file.jpg'
#puts file.inspect
pets = client.pet.find_by_status 'available'#Petstore::Api::Pet::STATUS['available']#'boring'
puts pets.inspect
