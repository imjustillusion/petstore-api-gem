# Petstore

simple api for http://petstore.swagger.io only for pets

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'petstore'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install petstore

## Usage

client = Petstore::Client.new('special-key')

## Example

```ruby
client = Petstore::Client.new('special-key')

# pet.add {params} - to add new pet
pet = client.pet.add {name: 'doggie', catalog: 'Catalog name', tags: ['one', 'two']}

# pet.update id, {params} - to update existing pet
updated_pet = client.pet.update pet[:id], {name: 'a little doggie'}

# pet.find id - to find a pet
finded_pet = client.pet.find updated_pet[:id]

# pet.set_image id, file, metadata - to set image for pet
client.pet.set_image finded_pet[:id], 'path/to/file.jpg'

# pet.remove id - to remove pet
client.pet.remove finded_pet[:id]

# pet.find_by_status status - to list pets with status
pets = client.pet.find_by_status 'available'
```





## Contributing

1. Fork it ( https://bitbucket.org/imjustillusion/petstore-api-gem/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
