require 'spec_helper'

describe Petstore::Client do

  let(:client) { Petstore::Client.new('special-key') }

  subject(:pet) {
    client.pet.add name: 'doggie', status: 'available'
  }

  it "should add pet with status 200" do
    expect(pet[:response].status).to eq(200)
  end

  it "should update existing pet" do
    expect(client.pet.update(pet[:id], name: 'little doggie')[:name]).to eq('little doggie')
  end

  it "should find pet" do
    expect(pet[:id]).to eq(client.pet.find(pet[:id])[:id])
  end

  it "should remove pet" do
    expect(client.pet.remove(pet[:id])[:response].status).to be(200)
  end

  it "should find by status" do
    expect(client.pet.find_by_status('boring')).not_to be_empty
  end

  describe "#set_image" do
    it "should upload image" do
      expect(client.pet.set_image(pet[:id], File.dirname(__FILE__) +'/../fixtures/image.jpg')[:response].status).to be(200)
    end
    it "should raise error" do
      expect{client.pet.set_image(pet[:id], File.dirname(__FILE__) +'/../fixtures/test.txt')}.to raise_error(Petstore::ArgumentError)
    end
  end

  describe 'errors' do
    it "should raise error with wrong params" do
      expect {client.pet.add(name: []) }.to raise_error(Petstore::ArgumentError)
    end
    it "should raise error with wrong status" do
      expect { client.pet.add(status: 'boring') }.to raise_error(Petstore::ArgumentError)
    end
    it "should raise error when delete pet with wrong id" do
      expect{client.pet.remove(0)}.to raise_error(Petstore::Error)
    end
  end
end
