shared_context "configuration" do
  let(:valid_api_key) { Hash[api_key: 'special-key'] }
  let(:invalid_api_key) { Hash[api_key: ''] }
end
