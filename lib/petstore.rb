require 'petstore/version'
require 'petstore/client'

module Petstore

  def self.client api_key
    @client ||= Petstore::Client.new api_key
  end
end
