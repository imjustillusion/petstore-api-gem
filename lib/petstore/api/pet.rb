require 'petstore/helper'
require 'json'

module Petstore
  module Api
    class Pet
      PATH = '/v2/pet'
      STATUSES =  ['available','pending','sold']
      ATTR = {name: String, catalog: [Hash, Integer, String], photoUrls: Array, tags: Array, status: String}

      def initialize client
        @client = client
      end

      def add params
        resp = @client.get_connection.post PATH, validate(params)
        Petstore::Client.response(resp)
      end

      def update id, params
        old_record = find id
        resp = @client.get_connection.put PATH, old_record.merge(validate(params))
        Petstore::Client.response(resp)
      end

      def find id
        resp = @client.get_connection.get "#{PATH}/#{id}"
        Petstore::Client.response(resp)
      end

      def find_by_status status
        resp = @client.get_connection.get "#{PATH}/findByStatus", {status: status}
        Petstore::Client.response(resp)
      end

      def remove id
        resp = @client.get_connection.delete "#{PATH}/#{id}"
        Petstore::Client.response(resp)
      end

      def set_image id, file_path, metadata = ""
        mime_type = FileMagic.new(FileMagic::MAGIC_MIME_TYPE).file(file_path)
        fail ArgumentError, 'file dosent exists or wrong mime type' unless File.exist?(file_path) and Petstore::Configuration::ALLOWED_FILE_TYPES.include?(mime_type)
        resp = @client.get_connection(true).post "#{PATH}/#{id}/uploadImage", {petId: id, file: Faraday::UploadIO.new(file_path, mime_type), additionalMetadata: metadata }
        Petstore::Client.response(resp)
      end

      private
      def validate params
        out = {}
        params.each do | key,param |
          raise ArgumentError, "wrong param #{param}" unless ATTR.keys.include?(key)
          case key
            when :catalog
              raise ArgumentError, "wrong param #{param} type. it must be #{ATTR[key.to_sym]}" unless ATTR[key.to_sym].include?(param.class)
              param = Petstore::Helper.params_to_hash param
            when :tags
              param = param.map {|tag| Petstore::Helper.params_to_hash tag}
            when :status
              raise ArgumentError, "wrong status. change to available stauses: #{STATUSES}" unless STATUSES.include?(param)
            else
              raise ArgumentError, "wrong type for #{param}" unless ATTR[key.to_sym] === param
          end
          out[key.to_sym]= param
        end
        out
      end
    end
  end
end
