module Petstore
  class Configuration
    API_HOST = 'http://petstore.swagger.io'
    ALLOWED_FILE_TYPES = ['image/jpeg', 'image/gif','image/png']
  end
end
