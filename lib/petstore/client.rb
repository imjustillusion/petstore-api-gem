require 'faraday'
require 'faraday_middleware'
require 'ruby-filemagic'

require 'petstore/configuration'
require 'petstore/error'
require 'petstore/api/pet'

module Petstore
  class Client

    REQUEST_CLASSES = {
      pet: Petstore::Api::Pet
    }
    def initialize(api_key)
      @api_key = api_key
      define_request_methods
    end

    class << self

      def is_valid_response response
        if (200..399).include?(response.status)
          return true
        else
            fail Error, "status: #{response.status}. response: #{response.inspect}" #response.inspect
        end
      end


      def response resp
        self.is_valid_response resp
        case resp.body
          when Hash
            body = resp.body.deep_symbolize_keys # convert string keys to symbols\
            body.merge!({response: resp})
          when Array
            body = {
              items: resp.body.map{ |e| e.deep_symbolize_keys },
              response: resp
            }
          else
            body = {body: resp.body, response: resp}
        end
        body
      end
    end
    def get_connection multipart=false
      Faraday.new(url: Petstore::Configuration::API_HOST, headers: { accept: 'application/json' }) do |conn|
        conn.request :oauth2, @api_key
        #conn.response :logger                  # log requests to STDOUT
        if multipart
           conn.request :multipart
           conn.request  :url_encoded             # form-encode POST params
        end
        #conn.response :json, :content_type => /\bjson$/
        conn.use FaradayMiddleware::EncodeJson
        conn.use FaradayMiddleware::ParseJson, :content_type => /\bjson$/
        conn.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      end
    end

    private
    # создаем метод CLASS_NAME с объектом из константы REQUEST_CLASSES
    # key = method_name, value = class
    # client.PET.add,
    #где client - new self, PET - alias из REQUEST_CLASSES, add - метод из PET
    def define_request_methods
      REQUEST_CLASSES.each do |index,request_class|
        define_singleton_method(index) do |*args|
          request_class.new(self)
        end
      end
    end
  end
end
