module Petstore
  # Custom error class for rescuing from all errors
  class Error < StandardError; end

  # Raised if returns HTTP status code 400
  class BadRequest < Error; end

  # Raised if returns the HTTP status code 404
  class NotFound < Error; end

  # Raised if returns the HTTP status code 500
  class InternalServerError < Error; end

  class ArgumentError < Error; end
end
